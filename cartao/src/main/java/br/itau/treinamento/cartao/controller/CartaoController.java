package br.itau.treinamento.cartao.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.cartao.model.dto.CartaoRequest;
import br.itau.treinamento.cartao.model.dto.CartaoResponse;
import br.itau.treinamento.cartao.model.dto.CartaoResponseResumido;
import br.itau.treinamento.cartao.model.dto.NumeroCartaoResponse;
import br.itau.treinamento.cartao.model.dto.StatusCartaoRequest;
import br.itau.treinamento.cartao.model.dto.StatusCartaoResponse;
import br.itau.treinamento.cartao.model.mapper.CartaoMapper;
import br.itau.treinamento.cartao.service.CartaoService;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;
    
    Logger logger = LoggerFactory.getLogger(CartaoController.class);


    @PostMapping
    @ResponseStatus(code= HttpStatus.CREATED)
    public CartaoResponse insereCartao(@Valid @RequestBody CartaoRequest cartaoRequest) {

        logger.debug("Insere cartao "  +cartaoRequest.getNumero());
        return CartaoMapper.cartaoResponsefromCartao(
                   cartaoService.insereCartao(
                           CartaoMapper.cartaofromCartaoRequest(cartaoRequest)));
    }

    @GetMapping("/{numero}")
    public CartaoResponseResumido buscaCartao(@Valid @PathVariable (value="numero") String numero) {

        logger.debug("Busca cartao " +numero);
        return CartaoMapper.cartaoResponseResumidofromCartao(
        		cartaoService.buscaCartao(numero));           
    }
 

    @PatchMapping("/{numero}")
    public CartaoResponse atualizaStatusCartao(@Valid @PathVariable(value="numero") String numero , @RequestBody StatusCartaoRequest statusCartaoRequest) {

        logger.debug("Atualiza status cartao " +numero);
        return CartaoMapper.cartaoResponsefromCartao(
        		cartaoService.atualizaCartao(statusCartaoRequest.getAtivo(),numero));        
    }

    @GetMapping("/{numero}/status")
    public StatusCartaoResponse consultaStatus (@Valid @PathVariable(value="numero") String numero){

        logger.debug("Consulta status cartao " +numero);       
        return CartaoMapper.statusCartaoResponsefromCartao(
        		cartaoService.buscaCartao(numero));
      
    }

    @GetMapping("/{cartao_id}/numero")
    public NumeroCartaoResponse consultaNumero (@Valid @PathVariable(value="cartao_id") Long cartaoId){

        logger.debug("Consulta numero cartao " +cartaoId);
        return CartaoMapper.numeroCartaoResponsefromCartao(cartaoService.buscaPorId(cartaoId));
       
    }

}
