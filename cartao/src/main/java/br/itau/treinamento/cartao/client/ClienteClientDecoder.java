package br.itau.treinamento.cartao.client;

import br.itau.treinamento.cartao.exception.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String s, Response response) {
		if(response.status()==404) {
			return new ClienteNotFoundException();
		}
		return null;
	}

}
