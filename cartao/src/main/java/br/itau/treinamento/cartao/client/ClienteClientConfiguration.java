package br.itau.treinamento.cartao.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

import feign.Feign;
import feign.RequestInterceptor;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;

public class ClienteClientConfiguration {
	
	@Autowired
	private OAuth2ClientContext clientContext;

	@Autowired
	private ClientCredentialsResourceDetails clientCredentialsResourceDetails;

	
	@Bean
	public ErrorDecoder getClienteClientDecoder() {
		return new ClienteClientDecoder();
	}
	
	@Bean
	public Feign.Builder builder(){
		FeignDecorators decorator = FeignDecorators.builder()
				.withFallback(new ClienteClientFallback(),RetryableException.class)
				.build();
	
		return Resilience4jFeign.builder(decorator);
	
	}
	
	@Bean
    public RequestInterceptor oauth2FeignRequestInterceptor() {
        return new OAuth2FeignRequestInterceptor(clientContext, clientCredentialsResourceDetails);
    }



}
