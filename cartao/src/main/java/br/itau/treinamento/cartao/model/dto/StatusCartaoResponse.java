package br.itau.treinamento.cartao.model.dto;

import javax.validation.constraints.NotNull;

public class StatusCartaoResponse {

    Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
