package br.itau.treinamento.cartao.model.mapper;


import br.itau.treinamento.cartao.model.Cartao;
import br.itau.treinamento.cartao.model.dto.*;

public class CartaoMapper{

    public static Cartao cartaofromCartaoRequest(CartaoRequest cartaoRequest){
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoRequest.getNumero());
        cartao.setAtivo(false);
        cartao.setClienteId(cartaoRequest.getClienteId());
        return cartao;
    }

    public static CartaoResponse cartaoResponsefromCartao(Cartao cartao){
        CartaoResponse cartaoResponse = new CartaoResponse();
        cartaoResponse.setAtivo(cartao.getAtivo());
        cartaoResponse.setClienteId(cartao.getClienteId());
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        return cartaoResponse;
    }

    public static CartaoResponseResumido cartaoResponseResumidofromCartao(Cartao cartao){
        CartaoResponseResumido cartaoResponse = new CartaoResponseResumido();
        cartaoResponse.setClienteId(cartao.getClienteId());
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        return cartaoResponse;
    }

    public static StatusCartaoResponse statusCartaoResponsefromCartao(Cartao cartao){
        StatusCartaoResponse statusCartaoResponse = new StatusCartaoResponse();
        statusCartaoResponse.setAtivo(cartao.getAtivo());
        return statusCartaoResponse;
    }

    public static NumeroCartaoResponse numeroCartaoResponsefromCartao(Cartao cartao){
        NumeroCartaoResponse numeroCartaoResponse = new NumeroCartaoResponse();
        numeroCartaoResponse.setNumero(cartao.getNumero());
        return numeroCartaoResponse;
    }
}
