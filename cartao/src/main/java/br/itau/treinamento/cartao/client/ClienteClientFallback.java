package br.itau.treinamento.cartao.client;

import br.itau.treinamento.cartao.exception.CartaoUnavailable;

public class ClienteClientFallback implements ClienteClient {


	@Override
	public Cliente buscaCliente(Long cliente_id) {
		throw new CartaoUnavailable();
	}

}
