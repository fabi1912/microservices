package br.itau.treinamento.cartao.model.dto;

import javax.validation.constraints.NotNull;

public class CartaoRequest {

    @NotNull
    private String numero;
    @NotNull
    private Long clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
