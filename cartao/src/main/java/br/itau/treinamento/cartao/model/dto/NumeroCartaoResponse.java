package br.itau.treinamento.cartao.model.dto;


public class NumeroCartaoResponse {


    private String numero;


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}


