package br.itau.treinamento.cartao.service;

import br.itau.treinamento.cartao.client.ClienteClient;
import br.itau.treinamento.cartao.exception.CartaoNotFoundException;
import br.itau.treinamento.cartao.model.Cartao;
import br.itau.treinamento.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;
    
    @Autowired
    ClienteClient clienteClient;


    public Cartao insereCartao(Cartao cartao){
    	
        clienteClient.buscaCliente(cartao.getClienteId());    	
        return cartaoRepository.save(cartao);
        
    }

    public Cartao buscaCartao(String numero){
        Cartao cartao= cartaoRepository.buscaCartaoPeloNumero(numero);
       
        if(cartao==null) {
        	 throw new CartaoNotFoundException();
        }
        return cartao;
    }

    public Cartao buscaPorId(Long id){
        Optional<Cartao> cartao= cartaoRepository.findById(id);
        if(cartao.isPresent()){
            return cartao.get();
        }
        throw new CartaoNotFoundException();
    }

    public Cartao atualizaCartao(Boolean ativo, String numero){
        Cartao cartao ;

        cartao = cartaoRepository.buscaCartaoPeloNumero(numero);
        if(cartao!=null) {
            cartao.setAtivo(ativo);
            cartaoRepository.save(cartao); 
            return cartao;
        }
        
        throw new CartaoNotFoundException();
       
    }

}
