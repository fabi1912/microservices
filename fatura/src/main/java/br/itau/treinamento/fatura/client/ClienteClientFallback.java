package br.itau.treinamento.fatura.client;

import br.itau.treinamento.fatura.exception.ClienteUnavailable;

public class ClienteClientFallback implements ClienteClient {

	@Override
	public Cliente buscaCliente(Long cliente_id) {
		throw new ClienteUnavailable();
	}

}
