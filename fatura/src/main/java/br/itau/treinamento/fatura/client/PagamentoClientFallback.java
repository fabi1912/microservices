package br.itau.treinamento.fatura.client;

import java.util.List;

import br.itau.treinamento.fatura.exception.PagamentoUnavailable;

public class PagamentoClientFallback implements PagamentoClient {

	@Override
	public List<Pagamento> buscaPagamentosCartao(Long idCartao) {
		throw new PagamentoUnavailable();
	}

	@Override
	public void apagaPagamento(Long idCartao) {
		throw new PagamentoUnavailable();	
	}

	

}
