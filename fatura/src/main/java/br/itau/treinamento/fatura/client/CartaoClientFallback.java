package br.itau.treinamento.fatura.client;

import br.itau.treinamento.fatura.exception.CartaoUnavailable;

public class CartaoClientFallback implements CartaoClient {

	@Override
	public Cartao buscaCartao(String numero) {
		throw new CartaoUnavailable();
	}

	@Override
	public NumeroCartao consultaNumero(Long cartaoId) {
		throw new CartaoUnavailable();
	}

	@Override
	public Cartao atualizaStatusCartao(String numero, StatusCartaoRequest statusCartaoRequest) {
		throw new CartaoUnavailable();
	}

}
