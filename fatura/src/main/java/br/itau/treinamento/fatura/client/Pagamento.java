package br.itau.treinamento.fatura.client;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Pagamento {

    Long id;
    @JsonProperty(value= "cartao_id")
    Long cartaoId;
    String descricao;
    Double valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Pagamento(Long id, Long cartaoId, String descricao, Double valor) {
        this.id = id;
        this.cartaoId = cartaoId;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Pagamento() {
        super();
    }
}
