package br.itau.treinamento.fatura.repository;

import br.itau.treinamento.fatura.model.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura,Long> {
}
