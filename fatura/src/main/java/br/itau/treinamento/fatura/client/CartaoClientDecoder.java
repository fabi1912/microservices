package br.itau.treinamento.fatura.client;

import br.itau.treinamento.fatura.exception.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String s, Response response) {
		if(response.status()==404) {
			return new CartaoNotFoundException();
		}
		return null;
	}

}
