package br.itau.treinamento.fatura.model.mapper;

import br.itau.treinamento.fatura.client.Pagamento;
import br.itau.treinamento.fatura.model.Fatura;
import br.itau.treinamento.fatura.model.dto.FaturaResponse;
import br.itau.treinamento.fatura.model.dto.PagamentoResponse;

import java.util.ArrayList;
import java.util.List;

public class FaturaMapper {

    public static FaturaResponse fromFatura(Fatura fatura){
        FaturaResponse faturaResponse = new FaturaResponse();
        faturaResponse.setId(fatura.getId());
        faturaResponse.setPagoEm(fatura.getData());
        faturaResponse.setValorPago(fatura.getValor());
        return faturaResponse;
    }

    public static List<PagamentoResponse> fromPagamentoList(List<Pagamento> pagamentos){
        List<PagamentoResponse> listaPagamento = new ArrayList<PagamentoResponse>();
        pagamentos.forEach(
                n-> listaPagamento.add(
                        new PagamentoResponse(n.getId(),n.getCartaoId(),n.getDescricao(),n.getValor())));
        return listaPagamento;

    }

}
