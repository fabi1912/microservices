package br.itau.treinamento.fatura.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.fatura.client.Cartao;
import br.itau.treinamento.fatura.client.CartaoClient;
import br.itau.treinamento.fatura.client.Cliente;
import br.itau.treinamento.fatura.client.ClienteClient;
import br.itau.treinamento.fatura.client.NumeroCartao;
import br.itau.treinamento.fatura.client.Pagamento;
import br.itau.treinamento.fatura.client.PagamentoClient;
import br.itau.treinamento.fatura.client.StatusCartaoRequest;
import br.itau.treinamento.fatura.exception.FaturaNotAuthorizated;
import br.itau.treinamento.fatura.model.Fatura;
import br.itau.treinamento.fatura.repository.FaturaRepository;

@Service
public class FaturaService {

    @Autowired
    FaturaRepository faturaRepository;
    
    @Autowired
    PagamentoClient pagamentoClient;

    @Autowired
    CartaoClient cartaoClient;

    @Autowired
    ClienteClient clienteClient;


    public Fatura pagarFatura(Long clienteId,Long cartaoId ){
        
    	

        Cliente cliente = clienteClient.buscaCliente(clienteId);
        NumeroCartao numeroCartao =cartaoClient.consultaNumero(cartaoId);
        Cartao cartao = cartaoClient.buscaCartao(numeroCartao.getNumero());

        if(cartao.getClienteId()!= cliente.getId()){
            throw new FaturaNotAuthorizated();
        }

        List<Pagamento>pagamentoList = pagamentoClient.buscaPagamentosCartao(cartaoId);

        if(pagamentoList!=null && !pagamentoList.isEmpty()){
            pagamentoClient.apagaPagamento(cartaoId);
        }
    	
    	Fatura fatura = new Fatura();
        fatura.setData(new Date());
        fatura.setCartaoId(cartaoId);
        double valorTotal=0;

        if(pagamentoList!=null && !pagamentoList.isEmpty()){
            for(Pagamento pagamento:pagamentoList){
                valorTotal = valorTotal + pagamento.getValor();
            }
        }

        fatura.setValor(valorTotal);
        return faturaRepository.save(fatura);
    
    }
    
    
    public  List<Pagamento> recuperaFatura(Long clienteId,Long cartaoId) {
    	  Cliente cliente = clienteClient.buscaCliente(clienteId);
          NumeroCartao numeroCartao =cartaoClient.consultaNumero(cartaoId);
          Cartao cartao = cartaoClient.buscaCartao(numeroCartao.getNumero());

          if(cartao.getClienteId()!= cliente.getId()){
              throw new FaturaNotAuthorizated();
          }
          
          return pagamentoClient.buscaPagamentosCartao(cartaoId);
    }
    
    public void bloqueiaCartao(Long clienteId,Long cartaoId) {
    	
    	Cliente cliente = clienteClient.buscaCliente(clienteId);
         NumeroCartao numeroCartao =cartaoClient.consultaNumero(cartaoId);
         Cartao cartao = cartaoClient.buscaCartao(numeroCartao.getNumero());

         if(cartao.getClienteId()!= cliente.getId()){
             throw new FaturaNotAuthorizated();
         }

         StatusCartaoRequest statusCartaoRequest = new StatusCartaoRequest();
         statusCartaoRequest.setAtivo(false);
         cartaoClient.atualizaStatusCartao(cartao.getNumero() , statusCartaoRequest);

    }
    
    


}
