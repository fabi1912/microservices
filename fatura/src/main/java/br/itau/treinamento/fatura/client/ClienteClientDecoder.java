package br.itau.treinamento.fatura.client;

import br.itau.treinamento.fatura.exception.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String s, Response response) {
		if(response.status()==404) {
			return new ClienteNotFoundException();
		}
		return null;
	}

}
