package br.itau.treinamento.fatura.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.fatura.model.dto.BloqueioCartaoResponse;
import br.itau.treinamento.fatura.model.dto.FaturaResponse;
import br.itau.treinamento.fatura.model.dto.PagamentoResponse;
import br.itau.treinamento.fatura.model.mapper.FaturaMapper;
import br.itau.treinamento.fatura.service.FaturaService;

@RestController
@RequestMapping("/fatura")
public class FaturaController {


    @Autowired
    FaturaService faturaService;

    Logger logger = LoggerFactory.getLogger(FaturaController.class);


    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<PagamentoResponse> recuperaFatura(@Valid @PathVariable(value="cliente-id") Long clienteId,
                                                  @PathVariable(value="cartao-id") Long cartaoId){

        logger.debug("Recuperando fatura " + cartaoId);
        return FaturaMapper.fromPagamentoList(
        		faturaService.recuperaFatura(clienteId, cartaoId));
    }

    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public BloqueioCartaoResponse expirarCartao(@Valid @PathVariable(value="cliente-id") Long clienteId,
                                                @PathVariable(value="cartao-id") Long cartaoId) {
        
    	logger.debug("Expira cartao "+ cartaoId);
        faturaService.bloqueiaCartao(clienteId, cartaoId);
        return new BloqueioCartaoResponse();
    }

    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public FaturaResponse pagarFatura(@Valid @PathVariable(value="cliente-id") Long clienteId,
                                      @PathVariable(value="cartao-id") Long cartaoId) {

        logger.debug("Pagar fatura : "+ cartaoId);
        return FaturaMapper.fromFatura(
        		faturaService.pagarFatura(clienteId,cartaoId));
    
    }

}
