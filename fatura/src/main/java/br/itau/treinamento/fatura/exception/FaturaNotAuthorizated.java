package br.itau.treinamento.fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Ação não permitida")
public class FaturaNotAuthorizated extends RuntimeException {


}
