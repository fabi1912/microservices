package br.itau.treinamento.fatura.client;

import br.itau.treinamento.fatura.exception.PagamentoNaoProcessadoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PagamentoClientDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String s, Response response) {
		if(response.status()==422) {
			return new PagamentoNaoProcessadoException();
		}
		return null;
	}

}
