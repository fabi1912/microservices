package br.itau.treinamento.fatura.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name="CARTAO",configuration=CartaoClientConfiguration.class)
public interface CartaoClient {


    @GetMapping("/cartao/{numero}")
    Cartao buscaCartao(@PathVariable String numero);

    @GetMapping("/cartao/{cartao_id}/numero")
    public NumeroCartao consultaNumero ( @PathVariable(value="cartao_id") Long cartaoId);


    @PatchMapping("/cartao/{numero}")
     Cartao atualizaStatusCartao( @PathVariable String numero , @RequestBody StatusCartaoRequest statusCartaoRequest) ;



    }
