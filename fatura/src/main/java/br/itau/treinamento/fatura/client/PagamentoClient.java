package br.itau.treinamento.fatura.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name="PAGAMENTO" ,configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {


    @GetMapping("/pagamento/{id_cartao}")
    public List<Pagamento>buscaPagamentosCartao(@PathVariable(value="id_cartao") Long idCartao);

    @DeleteMapping("/pagamento/{id_cartao}")
    public void apagaPagamento(@PathVariable(value="id_cartao") Long idCartao);

}
