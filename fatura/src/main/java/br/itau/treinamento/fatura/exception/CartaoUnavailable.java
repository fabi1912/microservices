package br.itau.treinamento.fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Servico de cartão indisponivel")
public class CartaoUnavailable extends RuntimeException {


}
