package br.itau.treinamento.cliente.model.mapper;


import br.itau.treinamento.cliente.model.Cliente;
import br.itau.treinamento.cliente.model.dto.ClienteRequest;
import br.itau.treinamento.cliente.model.dto.ClienteResponse;

public class ClienteMapper {

    public  static Cliente fromClienteRequest(ClienteRequest clienteRequest){

        Cliente cliente = new Cliente();
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

    public static ClienteResponse fromCliente(Cliente cliente){
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setName(cliente.getName());
        clienteResponse.setId(cliente.getId());
        return clienteResponse;
    }
}
