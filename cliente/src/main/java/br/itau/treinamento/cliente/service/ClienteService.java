package br.itau.treinamento.cliente.service;

import br.itau.treinamento.cliente.exception.ClienteNotFoundException;
import br.itau.treinamento.cliente.model.Cliente;
import br.itau.treinamento.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente insereCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscaCliente(Long id){
        Optional<Cliente> cliente= clienteRepository.findById(id);
        if(cliente.isPresent()){
            return cliente.get();
        }
        throw new ClienteNotFoundException();
    }
}
