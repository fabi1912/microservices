package br.itau.treinamento.cliente.controller;

import br.itau.treinamento.cliente.model.dto.ClienteRequest;
import br.itau.treinamento.cliente.model.dto.ClienteResponse;
import br.itau.treinamento.cliente.model.mapper.ClienteMapper;
import br.itau.treinamento.cliente.service.ClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    Logger logger = LoggerFactory.getLogger(ClienteController.class);


    @PostMapping
@ResponseStatus(code= HttpStatus.CREATED)
public ClienteResponse insereCliente (@Valid @RequestBody ClienteRequest clienteRequest){

        logger.debug("Insere cliente " + clienteRequest.getName());


    return ClienteMapper.fromCliente(
            clienteService.insereCliente(
                    ClienteMapper.fromClienteRequest(clienteRequest)));
}

@GetMapping("/{id}")
public ClienteResponse buscaCliente(@Valid @PathVariable(value="id") Long id){

    logger.debug("Consulta cliente " + id);

    return ClienteMapper.fromCliente(clienteService.buscaCliente(id));
}

}
