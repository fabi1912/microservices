package br.itau.treinamento.cliente.repository;

import br.itau.treinamento.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Long> {


}
