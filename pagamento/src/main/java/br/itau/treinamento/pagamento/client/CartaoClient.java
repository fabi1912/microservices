package br.itau.treinamento.pagamento.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;

@FeignClient(name="CARTAO",configuration = CartaoClientConfiguration.class)
public interface CartaoClient {



    @GetMapping("/cartao/{numero}/status")
    StatusCartao buscaStatus(@PathVariable String numero);

    @GetMapping("/cartao/{cartao_id}/numero")
    public NumeroCartao consultaNumero ( @PathVariable(value="cartao_id") Long cartaoId);


}
