package br.itau.treinamento.pagamento.client;

public class StatusCartao {

    Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
