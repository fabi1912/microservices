package br.itau.treinamento.pagamento.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {

    @NotNull
    @Column(name = "CARTAO_ID")
    private Long cartaoId;

    @Id
    @Column(name="PAGAMENTO_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name="PAGAMENTO_DESCRICAO")
    private String descricao;

    @NotNull
    @Column(name="PAGAMENTO_VALOR")
    private Double valor;

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
