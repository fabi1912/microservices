package br.itau.treinamento.pagamento.service;

import br.itau.treinamento.pagamento.client.CartaoClient;
import br.itau.treinamento.pagamento.client.NumeroCartao;
import br.itau.treinamento.pagamento.client.StatusCartao;
import br.itau.treinamento.pagamento.exception.PagamentoNaoProcessadoException;
import br.itau.treinamento.pagamento.model.Pagamento;
import br.itau.treinamento.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;
    
    @Autowired
    CartaoClient cartaoClient;


    public Pagamento inserePagamento(Pagamento pagamento){    	
        NumeroCartao numeroCartao =cartaoClient.consultaNumero(pagamento.getCartaoId());
        StatusCartao statusCartao= cartaoClient.buscaStatus(numeroCartao.getNumero());

        if(statusCartao.getAtivo()){
        	return pagamentoRepository.save(pagamento);
        }
        
        throw new PagamentoNaoProcessadoException();
        
    }

    public List<Pagamento> recuperaPagamentosCartao(Long idCartao){
        cartaoClient.consultaNumero(idCartao);
        return (List<Pagamento>) pagamentoRepository.recuperaPagamentos(idCartao);
    }

    public void removePagamentos(Long cartaoId){
    	cartaoClient.consultaNumero(cartaoId);
        pagamentoRepository.removeLancamentos(cartaoId);
    }

}
