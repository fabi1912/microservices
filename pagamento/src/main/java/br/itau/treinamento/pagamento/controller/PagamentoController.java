package br.itau.treinamento.pagamento.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.pagamento.client.CartaoClient;
import br.itau.treinamento.pagamento.model.dto.PagamentoRequest;
import br.itau.treinamento.pagamento.model.dto.PagamentoResponse;
import br.itau.treinamento.pagamento.model.mapper.PagamentoMapper;
import br.itau.treinamento.pagamento.service.PagamentoService;


@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    CartaoClient cartaoClient;

    Logger logger = LoggerFactory.getLogger(PagamentoController.class);



    @ResponseStatus(code= HttpStatus.CREATED)
    @PostMapping
    public PagamentoResponse inserePagamento(@Valid @RequestBody PagamentoRequest pagamentoRequest){

     logger.debug("Insere Pagamento " + pagamentoRequest.getDescricao());      
      return PagamentoMapper.fromPagamento(pagamentoService.inserePagamento(
                    PagamentoMapper.fromPagamentoRequest(pagamentoRequest)));
        
    }

    @GetMapping("/{id_cartao}")
    public List<PagamentoResponse> buscaPagamentosCartao(@PathVariable(value="id_cartao") Long idCartao){

        logger.debug("Busca Pagamento " + idCartao);
        return   PagamentoMapper.fromPagamentoList(
                    pagamentoService.recuperaPagamentosCartao(idCartao));

    }

    @DeleteMapping("/{id_cartao}")
    public void apagaPagamento(@PathVariable (value = "id_cartao")Long idCartao){

        logger.debug("Apaga Pagamento " + idCartao);      
        pagamentoService.removePagamentos(idCartao);
    }

}
