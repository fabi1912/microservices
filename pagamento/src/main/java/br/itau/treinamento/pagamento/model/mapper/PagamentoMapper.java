package br.itau.treinamento.pagamento.model.mapper;

import br.itau.treinamento.pagamento.model.Pagamento;
import br.itau.treinamento.pagamento.model.dto.PagamentoRequest;
import br.itau.treinamento.pagamento.model.dto.PagamentoResponse;

import java.util.ArrayList;
import java.util.List;

public class PagamentoMapper {

    public static Pagamento fromPagamentoRequest(PagamentoRequest pagamentoRequest){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoRequest.getDescricao());
        pagamento.setValor(pagamentoRequest.getValor());
        pagamento.setCartaoId(pagamentoRequest.getCartaoId());
        return pagamento;
    }

    public static PagamentoResponse fromPagamento(Pagamento pagamento){
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(pagamento.getCartaoId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }

    public static List<PagamentoResponse> fromPagamentoList(List<Pagamento> pagamentos){
        List<PagamentoResponse> listaPagamento = new ArrayList<PagamentoResponse>();
        pagamentos.forEach(
                n-> listaPagamento.add(
                        new PagamentoResponse(n.getId(),n.getCartaoId(),n.getDescricao(),n.getValor())));
        return listaPagamento;

    }

}
