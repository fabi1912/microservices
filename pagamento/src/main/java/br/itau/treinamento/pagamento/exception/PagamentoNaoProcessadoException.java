package br.itau.treinamento.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Cartao invalido para pagamento")
public class PagamentoNaoProcessadoException extends RuntimeException{

}
