package br.itau.treinamento.pagamento.client;

import br.itau.treinamento.pagamento.exception.CartaoUnavailable;

public class CartaoClientFallback implements CartaoClient {

	@Override
	public StatusCartao buscaStatus(String numero) {
		throw new CartaoUnavailable();

	}

	@Override
	public NumeroCartao consultaNumero(Long cartaoId) {
		throw new CartaoUnavailable();

	}


}
